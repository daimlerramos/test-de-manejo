package com.example.testdemanejo;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.sax.StartElementListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class p1 extends AppCompatActivity {
    private TextView titulo;
    private ImageView imagen;
    private Button btn;
    private RadioButton rb1, rb2, rb3, rb4;
    private int conta = 0;
    private RadioGroup radio;
    private int cuenta = 0;
    //-------------------------------------------------------------------------generacion de numeros randoms

    Random aleatorio = new Random(System.currentTimeMillis());

    int intAleatorio = aleatorio.nextInt(29);


    int min = 0;
    int max = 29;

    Random r = new Random(System.currentTimeMillis());
    int i1 = r.nextInt(max - min + 1) + min;
    //-----------------------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p1);
        //----------------------------------------------------------pantalla completa------------------------------------------------


        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.activity_p1);
        // Si la versión android es menor que Jellybean, usa este llamado para esconder la barra de estatus.

        titulo = findViewById(R.id.titulo);
        imagen = findViewById(R.id.imagen);
        btn = findViewById(R.id.btn);
        rb1 = findViewById(R.id.rb1);
        rb2 = findViewById(R.id.rb2);
        rb3 = findViewById(R.id.rb3);
        rb4 = findViewById(R.id.rb4);
        radio = findViewById(R.id.radio);

        btn.setText("Siguiente");
        titulo.setVisibility(View.VISIBLE);
        imagen.setVisibility(View.VISIBLE);
        radio.setVisibility(View.VISIBLE);
        Drawable img1 = getResources().getDrawable(R.drawable.img1);
        Drawable img2 = getResources().getDrawable(R.drawable.img2);
        Drawable img3 = getResources().getDrawable(R.drawable.img3);
        Drawable img4 = getResources().getDrawable(R.drawable.img4);
        Drawable img5 = getResources().getDrawable(R.drawable.img5);
        Drawable img6 = getResources().getDrawable(R.drawable.img6);
        Drawable img7 = getResources().getDrawable(R.drawable.img7);
        Drawable img8 = getResources().getDrawable(R.drawable.img8);
        Drawable img9 = getResources().getDrawable(R.drawable.img9);
        Drawable img10 = getResources().getDrawable(R.drawable.img10);
        Drawable img11 = getResources().getDrawable(R.drawable.img11);
        Drawable img12 = getResources().getDrawable(R.drawable.img12);
        Drawable img13 = getResources().getDrawable(R.drawable.img13);
        Drawable img14 = getResources().getDrawable(R.drawable.img14);
        Drawable img15 = getResources().getDrawable(R.drawable.img15);
        Drawable img16 = getResources().getDrawable(R.drawable.img16);
        Drawable img17 = getResources().getDrawable(R.drawable.img17);
        Drawable img18 = getResources().getDrawable(R.drawable.img18);
        Drawable img19 = getResources().getDrawable(R.drawable.img19);
        Drawable img20 = getResources().getDrawable(R.drawable.img20);
        Drawable img21 = getResources().getDrawable(R.drawable.img21);
        Drawable img22 = getResources().getDrawable(R.drawable.img22);
        Drawable img23 = getResources().getDrawable(R.drawable.img23);
        Drawable img24 = getResources().getDrawable(R.drawable.img24);
        Drawable img25 = getResources().getDrawable(R.drawable.img25);
        Drawable img26 = getResources().getDrawable(R.drawable.img26);
        Drawable img27 = getResources().getDrawable(R.drawable.img27);
        Drawable img28 = getResources().getDrawable(R.drawable.img28);
        Drawable img29 = getResources().getDrawable(R.drawable.img29);
        Drawable img30 = getResources().getDrawable(R.drawable.img30);

        //----------------------------------------------------------------------------------------------------------

        // Metemos en una lista los números del 1 al 40.
        List<Integer> numbers = new ArrayList<>(30);
        for (int i = 0; i < 29; i++) {
            numbers.add(i);
        }

// Instanciamos la clase Random
        Random random = new Random();

// Mientras queden cartas en el mazo (en la lista de numbers)
        while (numbers.size() > 0) {
            // Elegimos un índice al azar, entre 0 y el número de cartas que quedan por sacar
            int randomIndex = random.nextInt(numbers.size());

            // Damos la carta al jugador (sacamos el número por pantalla)


            //System.out.println("Not Repeated Random Number "+numbers.get(randomIndex));
            switch (randomIndex) {
                case 0:

                    titulo.setText("¿Cómo interpretaria la siguiente señal?");

                    imagen.setImageDrawable(img1);
                    rb1.setText("No se permite animales en la vía");
                    rb2.setText("Posibilidad de transito de animales en la Vía");
                    rb3.setText("Solo circulan animales en la vía");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;


                case 1:

                    titulo.setText("Si usted circula en la carretera, la señal que muestra la figura es:");

                    imagen.setImageDrawable(img2);
                    rb1.setText("Se aproximan peatones");
                    rb2.setText("Se aproxima a centro escolar ");
                    rb3.setText("No se aproxima centro escolar ");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;

                    break;

                case 2:
                    titulo.setText("¿Que notifica la señal que se muestra?");

                    imagen.setImageDrawable(img3);
                    rb1.setText("No tiene significado alguno");
                    rb2.setText("Que es permitido adelantar a otro vehículo ");
                    rb3.setText("Que está prohibido adelantar otros vehículos en determinados tramos de la vía");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb3.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;

                case 3:
                    titulo.setText("Los vehículos de transporte selectivo, deben poseer");

                    imagen.setImageDrawable(img4);
                    rb1.setText("Placa única adelante y placa de transporte público en la parte trasera");
                    rb2.setText("Las dos placas (única y de transporte público) en la parte trasera ");
                    rb3.setText("Placa única en la parte trasera ");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;

                case 4:
                    titulo.setText("Usted nunca debería adelantar un ciclista");

                    imagen.setImageDrawable(img5);
                    rb1.setText("Justo antes de doblar a la derecha");
                    rb2.setText("Justo antes de doblar a la izquierda ");
                    rb3.setText("Recien pasada una intersección");
                    rb4.setText("En un camino de tierra");
                    if (rb1.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 5:
                    titulo.setText("Son prohibiciones en relación a las placas únicas de circulación");

                    imagen.setImageDrawable(img6);
                    rb1.setText("Remplazar la placa oficial por otra con diseños diferentes al confeccionado por el estado");
                    rb2.setText("Colocar la placa de circulación vehicular en un lugar distinto al establecido, o de tal manera que se dificulte o impida su legibilidad ");
                    rb3.setText("Transitar con placa vencida");
                    rb4.setText("Todas las anteriores");
                    if (rb4.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 6:
                    titulo.setText("El informe escrito de lo acontecido en un accidente de tránsito, elaborado por el inspector de tránsito posee.");

                    imagen.setImageDrawable(img7);
                    rb1.setText("Nombre de los lesionados, o de los fallecidos si los hubiereo");
                    rb2.setText("Descripción de los daños visibles a vehículos y/o propiedad pública y privada ");
                    rb3.setText("Relatos de los hechos ocurridos");
                    rb4.setText("Todas las anteriores");
                    if (rb4.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 7:
                    titulo.setText("Cuando usted sale con su vehículo después de estar parqueado a la orilla de una cuneta. ¿Qué debe hacer?");

                    imagen.setImageDrawable(img8);
                    rb1.setText("Enciende la señal, observa hacia adelante y sale con precaución");
                    rb2.setText("Enciende la señal, hace uso de los espejos retrovisores, observa hacia atrás por su hombro, y sale con precaución");
                    rb3.setText("Enciende la señal respectiva y sale con su vehículo");
                    rb4.setText("Enciende la señal y sale con su vehículo sin ninguna precaución");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 8:
                    titulo.setText("¿Qué debe de tener presente y cumplir cuando conduce un vehículo?");

                    imagen.setImageDrawable(img9);
                    rb1.setText("No estacionarse en curva");
                    rb2.setText("Aumentar la velocidad de su vehículo al atravesar la bocacalle");
                    rb3.setText("Conducir durante la noche con luz altas dentro de la ciudad");
                    rb4.setText("Dejar el vehículo estacionado a menos de 5 metros de la esquina, de las intersecciones urbana y de los hidrantes públicos");
                    if (rb1.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 9:
                    titulo.setText("Para los conductores de los vehículos es permitido ");

                    imagen.setImageDrawable(img10);
                    rb1.setText("Portar licencia de conducir deteriorada");
                    rb2.setText("Portar licencia de conducir no adecuada al vehículo");
                    rb3.setText("Portar licencia de conducir vencida, suspendida o cancelada");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb4.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 10:
                    titulo.setText("La señal “puente angosto” ¿Es una señal?");

                    imagen.setImageDrawable(img11);
                    rb1.setText("Preventiva");
                    rb2.setText("Restrictiva");
                    rb3.setText("Informativa");
                    rb4.setText("Dispositivo para protección de obras");
                    if (rb1.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 11:
                    titulo.setText("La señal “Zona escolar” ¿Es una señal?");

                    imagen.setImageDrawable(img12);
                    rb1.setText("Preventiva ");
                    rb2.setText("Restrictiva ");
                    rb3.setText("Informativa");
                    rb4.setText("Dispositivo para protección de obras");
                    if (rb1.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 12:
                    titulo.setText("Al conductor involucrado en un accidente y en esta de embriaguez o bajo efectos de droga será sancionado con:");

                    imagen.setImageDrawable(img13);
                    rb1.setText("Multa de B/ 150.00 ");
                    rb2.setText("Un mes de arresto v");
                    rb3.setText("3 a 5 años de cárcel");
                    rb4.setText("Suspención de licencia por un mes");
                    if (rb1.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 13:
                    titulo.setText("La señal que nos indica el peso permitido en un lugar determinado ¿Es una señal? ");

                    imagen.setImageDrawable(img14);
                    rb1.setText("Preventiva");
                    rb2.setText("Restrictiva");
                    rb3.setText("Informativa");
                    rb4.setText("Dispositivo para protección de obras");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 14:
                    titulo.setText("El vehículo será removido de la vía, cuándo");

                    imagen.setImageDrawable(img15);
                    rb1.setText("Cuándo no esté presente el conductor o propietario");
                    rb2.setText("Cuándo no quiera o no pueda mover el vehículo");
                    rb3.setText("El conductor del vehículo indebidamente estacionado este presente");
                    rb4.setText("a) y b) son correctas");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 15:
                    titulo.setText("En una zona de estacionamiento para personas con discapacidad");

                    imagen.setImageDrawable(img16);
                    rb1.setText("Es permitido estacionarse");
                    rb2.setText("Es prohibido estacionarse");
                    rb3.setText("Es permitido, contando con la debida identificación y autorización expedida por la autoridad competente");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb3.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 16:
                    titulo.setText("Los tipos de señales de tránsito se dividen en ");

                    imagen.setImageDrawable(img17);
                    rb1.setText("Preventivas y reglamentarias");
                    rb2.setText("Informativas y reglamentarias");
                    rb3.setText("Preventivas, reglamentarias e informativa");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb3.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 17:
                    titulo.setText("Su vehículo a quedado dañado o parado en medio de una autopista. ¿qué es lo primero que usted debería de hacer");

                    imagen.setImageDrawable(img18);
                    rb1.setText("Intentar reparar su vehículo rápidamente");
                    rb2.setText("Encender un triángulo reflectante para advertir a los demás usuarios");
                    rb3.setText("Intentar detener a los autos que pasan para solicitar ayuda ");
                    rb4.setText("Todas las anteriores");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 18:
                    titulo.setText("Mientras va conduciendo su vehículo usted siente que lo están llamando a su celular, Si el uso de ese no es por medio de un sistema de manos libres, qué hace para responder la llamada");

                    imagen.setImageDrawable(img19);
                    rb1.setText("Reduce su velocidad");
                    rb2.setText("Conduce con una mano el volante");
                    rb3.setText("Espera hasta encontrar un lugar seguro donde detenerse");
                    rb4.setText("Es especialmente cauteloso en las intersecciones");
                    if (rb3.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 19:
                    titulo.setText("En qué circunstancias usted se detendría justo sobre un cruce cebra existente a mitad de cuadra");

                    imagen.setImageDrawable(img20);
                    rb1.setText("En ningún momento");
                    rb2.setText("Cuando no hay peatones para cruzar ");
                    rb3.setText("Durante la noche");
                    rb4.setText("Cuándo ello sea necesario para evitar un accidente");
                    if (rb4.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 20:
                    titulo.setText("¿Cuál es el significado de la señal “cruce de caminos");

                    imagen.setImageDrawable(img21);
                    rb1.setText("Se acerca adonde otro carril se une al carril que usted lleva");
                    rb2.setText("Se acerca a un tramo donde circulan vehículos en los cuatro puntos cardinales");
                    rb3.setText("Tener cuidado con los conductores que pudieran aparecer de momento");
                    rb4.setText("Ninguna de las anteriores es correcta");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 21:
                    titulo.setText("¿Qué señales tienen por objeto indicar al usuario, limitaciones físicas o prohibiciones reglamentarias que regulan el trafico?");

                    imagen.setImageDrawable(img22);
                    rb1.setText("Las señales restrictivas ");
                    rb2.setText("Las señales de servicio");
                    rb3.setText("Las señales preventivas ");
                    rb4.setText("Las señales regulativas");
                    if (rb1.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 22:
                    titulo.setText("Cuando la luz del semáforo se encuentra de color amarillo, usted debe ");

                    imagen.setImageDrawable(img23);
                    rb1.setText("Aumentar la velocidad");
                    rb2.setText("Prepararse para detenerse");
                    rb3.setText("Mantener la misma velocidad ");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 23:
                    titulo.setText("Que debe hacer usted si al ir conduciendo observa que una persona está cruzando por el paso peatonal");

                    imagen.setImageDrawable(img24);
                    rb1.setText("Pitar para que se apure");
                    rb2.setText("Detenerse con precaución");
                    rb3.setText("Acelerar para esquivar");
                    rb4.setText("Detenerse repentinamente");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 24:
                    titulo.setText("Usted se detiene ante un cruce de cebra, en la vereda hay peatones esperando, pero ellos no comienzan a cruzar. ¿Qué hace usted?");

                    imagen.setImageDrawable(img25);
                    rb1.setText("Tiene paciencia y espera");
                    rb2.setText("Les toca la bocina para apurarlo");
                    rb3.setText("Prosigue su marcha");
                    rb4.setText("Les hace seña con la mano, apurándolos para que crucen");
                    if (rb1.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 25:
                    titulo.setText("Para los vehículos de carga que transportan material de polvo, susceptible de ser esparcida por el viento, deberán de tomar las siguientes medidas");

                    imagen.setImageDrawable(img26);
                    rb1.setText("Transitar solo de noche");
                    rb2.setText("Llevarlo bajo cubierta protectora");
                    rb3.setText("Llevarlo descubierto ");
                    rb4.setText("Amarrarlo bien");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 26:
                    titulo.setText("Mencione un lugar donde es prohibido parar y estacionarse ");

                    imagen.setImageDrawable(img27);
                    rb1.setText("En el carril derecho");
                    rb2.setText("En los estacionamientos");
                    rb3.setText("En una curva");
                    rb4.setText("a) y b) son correctas");
                    if (rb3.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 27:
                    titulo.setText("Los conductores precavidos no manejan cuando ");

                    imagen.setImageDrawable(img28);
                    rb1.setText("Hay mucho trafico");
                    rb2.setText("Si está enfermo o enojado");
                    rb3.setText("Es de noche");
                    rb4.setText("Ninguna de las anteriores");
                    if (rb2.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 28:
                    titulo.setText("Si usted desea rebasar (pasar) a otro vehículo que va en la misma dirección, porque lado lo haría");

                    imagen.setImageDrawable(img29);
                    rb1.setText("Por el lado izquierdo ");
                    rb2.setText("No importa a qué lado ");
                    rb3.setText("Por el lado derecho");
                    rb4.setText("No se puede hacer");
                    if (rb1.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;
                case 29:
                    titulo.setText("Quién tiene la prioridad de paso en los redondeles");

                    imagen.setImageDrawable(img30);
                    rb1.setText("Los que van ingresar");
                    rb2.setText("El que entre más rápido");
                    rb3.setText("Los que ya están dentro ");
                    rb4.setText("Ninguna es correcta");
                    if (rb3.isChecked() == true) {
                        conta = conta + 1;
                    } else conta = conta + 0;
                    cuenta = cuenta + 1;
                    break;


            }

            // Y eliminamos la carta del mazo (la borramos de la lista)
            numbers.remove(randomIndex);
            if (cuenta == 30) {
                btn.setText("Finalizar");

            }


        }





        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }

           

        });
    }
}
