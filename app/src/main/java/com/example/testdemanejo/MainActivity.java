package com.example.testdemanejo;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
// Si la versión android es menor que Jellybean, usa este llamado para esconder la barra de estatus.
        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.activity_main);


        final TextView username =(TextView) findViewById(R.id.username);
        final TextView password =(TextView) findViewById(R.id.password);

        Button loginbtn =(Button) findViewById(R.id.loginbtn);

        //usuario y contraseña = admin

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(username.getText().toString().equals("admin") && password.getText().toString().equals("admin")){
                //correcto
                    Toast.makeText(MainActivity.this, "Inicio de sesión correcto",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(view.getContext(), Main2Activity.class);
                    startActivity(intent);
                }else
                    //incorrecto
                 Toast.makeText(MainActivity.this, "Contraseña o Usuario incorrecto",Toast.LENGTH_SHORT).show();

            }
        });

    }
}
